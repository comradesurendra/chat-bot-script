-------------------------------------------------------------------------new-script-chat-bot-----------------------------------------------
```
<script>
    var url = 'https://admiring-johnson-e1a6fa.netlify.app/script.js';
    var s = document.createElement('script');
    window.localStorage.setItem('pop_up',true);
    s.type = 'text/javascript';
    s.async = true;
    s.src = url;
    var options = {
  "enabled":true,
  "chatButtonSetting":{
      "backgroundColor":"#4dc247",
      "ctaText":"",
      "borderRadius":"25",
      "marginLeft":"20",
      "marginBottom":"10",
      "marginRight":"50",
      "position":"left"
  },
  "brandSetting":{
      "brandName":"DEMO",                                                   // Name of the hospital
      "brandSubTitle":"Typically replies instantly",                        
      "brandImg":"https://www.w3.org/TR/2017/CR-html52-20170808/images/clara.png",   // Hospital Logo image
      "welcomeText":"Hi there 👋\nHow can i help you?",                    // Text in the chatbot
      "messageText":"Hi",                                                  // Chnage this to send the message to the whats app 
      "backgroundColor":"#31326f",                                         // Change this to change the color of the background 
      "ctaText":"Start Chat",
      "borderRadius":"25",
      "autoShow":true,                                                     // Change this to flase to disable the auto pop up feature
      "phoneNumber":"919926761055"                                         // Need the whatsapp number of the client
  }
};
    s.onload = function() {
        CreateWhatsappChatWidget(options);

    };
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
</script>

```
